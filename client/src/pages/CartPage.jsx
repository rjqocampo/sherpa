import React, {useContext, useEffect, useState} from 'react'
import {GQLClient} from '../helpers'
import Query from '../graphql/queries'
import {UserContext} from '../contexts/UserContext'
import Mutation from '../graphql/mutations'

import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import style from '../style/CartPage.module.css'

const CartPage = () => {    
    return(
        <Container id={style.pageContainer}>
            <Card>
                <Card.Header id={style.cardHeader}>Following Activities</Card.Header>
                <Card.Body>
                    <CartList />
                </Card.Body>
            </Card>
        </Container>
    )
}

const CartList = () => {
    const { user } = useContext(UserContext)
	const [cartItems, setCartItems] = useState([])

    const deleteCart = (e, id) => {
        e.preventDefault()

        GQLClient({}).request(Mutation.deleteCart, { cartId: id }).then( (data) => getCartItems())
    }

    useEffect( () => {
        getCartItems()
    }, [])

    const getCartItems = () => {
        GQLClient({}).request(Query.getUserCart, { id: user.id }).then( (data) => {
            let cartRows = data.getUserCart.map( (cart) => 
                <ListGroup.Item>
                    <Row>
                        <Col md={9}>
                            <h3>{cart.activityName} </h3>
                            <h6>{cart.activityLocation} | {cart.activityDate}</h6>
                            <h6>&#8369;{cart.activityPrice}/person</h6>
                        </Col>
                        
                        <Col md={3} id={style.buttonContainer}>
                            
                            <Button id={style.buttonBook} href={ "/confirm-book/" + cart.activityId } variant="sm" className="btn-success btn-block">Proceed Booking</Button>
                            <Button id={style.buttonUnfollow} onClick={ (e) => deleteCart(e, cart.id) } variant="sm" className="btn-white btn-block">Unfollow</Button>
                        </Col>
                    </Row>
                </ListGroup.Item>
            )

            setCartItems(cartRows)
        })
    }

    return(
        <ListGroup>
            {cartItems}
        </ListGroup>
    )
}

export default CartPage