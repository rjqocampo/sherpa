import React, { useState, useEffect, useContext } from 'react'
import {UserContext} from '../contexts/UserContext'
import Query from '../graphql/queries'
import { GQLClient, NodeServerURL } from '../helpers'
import Mutation from '../graphql/mutations'

import Swal from 'sweetalert2'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Modal from 'react-bootstrap/Modal'
import LoginForm from './LoginPage.jsx'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { ReactComponent as SvgWave1 } from '../style/images/wave1.svg';
import { ReactComponent as SvgSafe } from '../style/images/safe.svg';
import { ReactComponent as SvgChecklist } from '../style/images/todo.svg';
import { ReactComponent as SvgGroufie } from '../style/images/groufie.svg';

import style from '../style/MenuPage.module.css'


const MenuPage = (props) => {
	const [activities, setActivities] = useState([])
	const {user} = useContext(UserContext)
	const btnAddActivity = <Button variant="success" href="/create" className="ml-auto float-right">Add Activity</Button>
	let LandingPage
	let TrioPage
	let ActivitiesTrioPage
	let ActivitiesLandingPage
	let MeetHostsPage
	
	if (user.token === null) {
		LandingPage = (
			<Landing />
		)

		TrioPage = (
			<Trio />
		)

		MeetHostsPage = (
			<MeetHosts />
		)
	} 

	if (user.role === 'customer') {
		ActivitiesLandingPage = (
			<ActivitiesHeader />
		)

		ActivitiesTrioPage = (
			<Trio />
		)

		MeetHostsPage = (
			<MeetHosts />
		)
	}

	useEffect( () => {
		GQLClient({}).request(Query.getActivities, null).then( ({getActivities}) => {
			setActivities(getActivities.map( (activity) => <ActivityItem key={activity.id} activity={activity}/> ))
		})
	}, [])

    return(
		<React.Fragment>
			<Container fluid className={style.landingPageContainer}>
				{ LandingPage }
			</Container>

			<Container fluid>
				{ ActivitiesLandingPage }
			</Container>

			<Container fluid>
				{ TrioPage }
			</Container>


			<Container className={style.activityListContainer}>
				<Row>
					<Col id={style.activityContainer}>
						<h1 id={style.activityHeader}>Activities  { (user.role === 'admin') ? btnAddActivity : null}</h1>
						<Row >
							{activities}
						</Row>
					</Col>
				</Row>
			</Container>

			<Container fluid>
				{ ActivitiesTrioPage }
			</Container>

			<Container fluid>
				{ MeetHostsPage }
			</Container>	
		</React.Fragment>
    )
}

const ActivitiesHeader = () => {
	return (
		<Row>
			<Col id={style.activitiesLandingPageContainer}>
				<div id={style.activitiesLandingPage}>
					<h1 id={style.activitiesLandingPageHeader}>Join one-of-a-kind activities led by amazing locals</h1>
				</div>
				<SvgWave1 id={style.svgWave} />
			</Col>
		</Row>
	)
}

const Trio = () => {
	return (
		<Row id={style.trioContainer}>
			<Col md={12}>
				<h1 id={style.trioMainHeader}>Let us do the planning for you</h1>
			</Col>
			<Col md={3}>
				<div id={style.svgTrioContainer}>
					<SvgGroufie id={style.svgTrio}/>
				</div>
				<h6 id={style.trioLabel}>Intimate Groups</h6>
				<h6 id={style.trioLabelDescription}>Join small intimate groups and do activities curated by expert locals who knows the area better than anyone.</h6>
			</Col>
			<Col md={3}>
				<div id={style.svgTrioContainer}>
					<SvgChecklist id={style.svgTrio}/>
				</div>
				<h6 id={style.trioLabel}>Book and go</h6>
				<h6 id={style.trioLabelDescription}>No need to worry about what to bring or where to eat. Your host prepares everything for you. Bring yourself and we're good to go.</h6>
			</Col>
			<Col md={3}>
				<div id={style.svgTrioContainer}>
					<SvgSafe id={style.svgTrio}/>
				</div>
				<h6 id={style.trioLabel}>Curated Activities</h6>
				<h6 id={style.trioLabelDescription}>
					Whether you're a beginner or an expert, all activities are accessible for everyone. We prioritize your safety and health above all else.
				</h6>
			</Col>
		</Row>
	)
}

const MeetHosts = () => {
	return (
		<Row id={style.meetHostsContainer}>
			<Col md={12}>
				<h1 id={style.meetHostsMainHeader}>Meet wonderful locals from around the world</h1>
			</Col>
			<Col md={2}>
				<div id={style.imgHostsContainer}>
					<img id={style.meetHostImage} alt="host preview" src={require("../style/images/host6.jpg")}/>
				</div>
				<h6 id={style.meetHostLabel}>Julie</h6>
				<h6 id={style.meetHostDescription}>The Master Potter</h6>
			</Col>
			<Col md={2}>
				<div id={style.imgHostsContainer}>
					<img id={style.meetHostImage2} alt="host preview" src={require("../style/images/host3.jpg")}/>
				</div>
				<h6 id={style.meetHostLabel}>Phillip</h6>
				<h6 id={style.meetHostDescription}>The Dauntless Photographer</h6>
			</Col>
			<Col md={2}>
				<div id={style.imgHostsContainer}>
					<img id={style.meetHostImage} alt="host preview" src={require("../style/images/host5.jpg")}/>
				</div>
				<h6 id={style.meetHostLabel}>Hannah</h6>
				<h6 id={style.meetHostDescription}>The Hanoi Food Expert</h6>
			</Col>
			<Col md={2}>
				<div id={style.imgHostsContainer}>
					<img id={style.meetHostImage2} alt="host preview" src={require("../style/images/host4.jpg")}/>
				</div>
				<h6 id={style.meetHostLabel}>Antonio</h6>
				<h6 id={style.meetHostDescription}>The King Bee</h6>
			</Col>
			<Col md={2}>
				<div id={style.imgHostsContainer}>
					<img id={style.meetHostImage} alt="host preview" src={require("../style/images/host7.jpg")}/>
				</div>
				<h6 id={style.meetHostLabel}>Adi</h6>
				<h6 id={style.meetHostDescription}>The Beach Bum of Quezon</h6>
			</Col>
		</Row>
	)
}


const Landing = () => {
	return(
		<Row id={style.landingPage}>
			<Col md={{ span: 4, offset: 1 }}>
				<LoginForm />
			</Col>
			<Col md={6}>
				<text className={style.landingPageHeader}>Discover experiences led by amazing locals </text>
			</Col>
			<SvgWave1 id={style.svgWave} />
		</Row>
	)
}

const ActivityItem = ({activity}) => {
	const {user} = useContext(UserContext)
	const [show, setShow] = useState(false);
	const [toggleFollow, setToggleFollow] = useState(false)

	const modalClose = () => setShow(false);
	const modalShow = () => setShow(true);

	let btnActivityActions = ''
	let btnModalActions = ''
	let btnHoverActions = ''

	if (user.role === 'admin') {
		btnModalActions = (
			<MDBContainer id={style.adminButtonsContainer}>
				<MDBBtn id={style.hoverAdminEdit} variant="success" href={ "/activity-edit/" + activity.id }><i class="fas fa-pen"></i></MDBBtn>
				<MDBBtn id={style.hoverAdminDelete} variant="danger" href={ "/activity-delete/" + activity.id }><i class="fas fa-trash-alt"></i></MDBBtn>
			</MDBContainer>
		)
	}

	if (user.token === null) {
		btnModalActions = (
			<div>
				<Button id={style.modalButtonRegister} href={ "/confirm-book/" + activity.id } size="sm" className="float-left" href="/">Login</Button>
			</div>
		)
	} else if (user.role === 'customer') {
		btnModalActions = (
			<div>
				<Button id={style.modalButtonCancel} variant="secondary" onClick={modalClose}>
					<i class="fas fa-times"></i>
				</Button>
				<Button id={style.modalButtonFollow} size="sm" onClick={ (e) => followActivity(e)}>
					{ (toggleFollow) ? <i class="fas fa-heart" id={style.modalHeartButton}></i> : <i class="fas fa-heart"></i> }
				</Button>
				<Button id={style.modalButtonBook} href={ "/confirm-book/" + activity.id } size="sm" onClick={modalClose}>
					Book
				</Button>
			</div>
		)

		btnHoverActions = (
			<a className={style.hoverOverlayButton} onClick={ (e) => followActivity(e)}>
				{ (toggleFollow) ? <i class="fas fa-heart" id={style.heartButton}></i> : <i class="far fa-heart" id={style.heartButton}></i> }
			</a>
		)
	}

	const followActivity = (e) => {
		e.preventDefault()
		e.stopPropagation()

		let variables = {
			userId: user.id,
			activityId: activity.id,
			activityName: activity.name,
			activityPrice: parseFloat(activity.price),
			activityDate: activity.activityDate,
			activityLocation: activity.location,
			activityImageLocation: activity.imageLocation
		}

		GQLClient({}).request(Mutation.addToCart, variables).then( (data) => {
			if (data.addToCart) {
				Swal.fire({
					title: 'Following Activity',
					icon: 'success'
				}) 
			} else {

			}
		})

		setToggleFollow(true)
	}

	return (
		<React.Fragment>
			<Col xs={6} md={3}>
				<Card className={style.cardContainer}>
					{btnActivityActions}
					
					<div className={style.hoverContainer}>
						<a onClick={modalShow}>
							<img className={style.cardImage} alt="activity preview" src={ NodeServerURL + activity.imageLocation }/>
							<div className={style.hoverOverlay}>
								{btnHoverActions}
								
								<div className={style.hoverOverlayTextGroup}>
									<p className={style.hoverOverlayText}>{ activity.tag }</p>
									<p className={style.hoverOverlayText}>{ activity.activityDate }</p>
									<p className={style.hoverOverlayText}>P{ activity.price }/person</p>
								</div>
							</div>
						</a>
					</div>
					<Card.Body className={style.cardBody}>
						<div>
							<strong id={style.activityName}>{ activity.name }</strong>
						</div>
						<div>
							<span id={style.activityLocation}>{activity.location}</span>
						</div>
					</Card.Body>
				</Card>
			</Col>

			<MDBModal animation="top" isOpen={show} toggle={modalClose} size="xl" centered>
				<MDBModalBody id={style.modalBody}>
					<Row>
						<Col md={5}>
							<img id={style.modalImage} alt="activity preview" src={ NodeServerURL + activity.imageLocation }/>
						</Col>

						<Col md={7} id={style.modalContainer} noGutters>
							<Row md={12} id={style.modalHeader} noGutters>
								<h2>{activity.name}</h2>
							</Row>
							<Row id={style.modalSubHeader} noGutters>
								<Col md={5}>
									<div>
										<span>{activity.location} | {activity.tag}</span>
									</div>
								</Col>
								<Col md={7} id={style.modalDate}>{activity.activityDate}</Col>
							</Row>
							<hr id={style.horizontalLine}></hr>
							<Row id={style.modalDescriptionContainer} noGutters>
								<Col md={3} id={style.modalWhat}>What you'll do</Col>
								<Col md={9} id={style.modalDescription}>{activity.description}</Col>
							</Row>
							<Row id={style.modalHostDetailsContainer} noGutters>
								<Col md={3} id={style.modalWho}>Your host</Col>
								<Col md={3} id={style.modalHostName}>{activity.hostName}</Col>
								<Col md={6} id={style.modalHostTitle}>{activity.hostTitle}</Col>
							</Row>
							<Row id={style.modalHostDescriptionContainer} noGutters>
								<Col md={{span: 9, offset: 3}} id={style.modalHostDescription}>{activity.hostDescription}</Col>
							</Row>
							<Row id={style.modalIconsContainer} noGutters>
								<Col md={{span: 9, offset: 3}}>
									<Row>
										<Col md={3}>
											<i id={style.modalIcons} class="fas fa-users"></i>
										</Col>
										<Col md={3}>
											<i id={style.modalIcons} class="fas fa-clock"></i>	
										</Col>
										<Col md={5}>
											<i id={style.modalIcons} class="fas fa-clipboard-list"></i>
										</Col>
									</Row>
								</Col>
							</Row>
							<Row id={style.modalMinorDetailsContainer} noGutters>
								<Col md={{span: 9, offset: 3}}>
									<Row>
										<Col md={3}>
											<span id={style.modalMinorDetailsHeader}>Group Size</span><br/>
											<span id={style.modalMinorDetailsContent}>{activity.groupSize}</span>
										</Col>
										<Col md={3}>
											<span id={style.modalMinorDetailsHeader}>Duration</span><br/>
											<span id={style.modalMinorDetailsContent}>{activity.duration}</span>
										</Col>
										<Col md={5}>
											<span id={style.modalMinorDetailsHeader}>Includes</span><br/>
											<span id={style.modalMinorDetailsContent}>Food and equipment</span>
										</Col>
									</Row>
								</Col>
							</Row>
							<Row id={style.modalButtonsContainer} noGutters>
								<Col md={{span: 9, offset: 3}}>
									<Row>
										<Col md={5}>
											<text id={style.modalPrice}>&#8369;{ activity.price }/person</text>
										</Col>
										<Col md={7}>
											{btnModalActions}
										</Col>
									</Row>
								</Col>
							</Row>
						</Col>
					</Row>
				</MDBModalBody>
			</MDBModal >

		</React.Fragment>
	)
}

export default MenuPage