import React, {useState, useEffect, useContext} from 'react'
import {UserContext} from '../contexts/UserContext'
import {GQLClient, NodeServerURL} from '../helpers'
import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'
import {Redirect} from 'react-router-dom'

import Swal from 'sweetalert2'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import style from '../style/BookingPage.module.css'

const BookingPage = (props) => {
	const [isRedirected, setIsRedirected] = useState(false)
	const { user } = useContext(UserContext)

	const [aName, setName] = useState('')
	const [aDescription, setDescription] = useState('')
	const [aTag, setTag] = useState('')
	const [aLocation, setLocation] = useState('')
	const [aDate, setDate] = useState('')
	const [aGroupSize, setGroupSize] = useState('')
	const [aPrice, setPrice] = useState('')
	const [aImageLoc, setImageLocation] = useState('')
	const [hTitle, setHostTitle] = useState('')
	const [hDescription, setHostDescription] = useState('')
	const [hName, setHostName] = useState('')
	const [aDuration, setDuration] = useState('')

	const options = {
        headers: {
            Authorization: 'Bearer ' + user.token
        }
	}
	
	useEffect( () => {
		GQLClient({}).request(Query.getActivity, { id: props.match.params.id }).then( (data) => {
			setName(data.getActivity.name)
			setDescription(data.getActivity.description)
			setTag(data.getActivity.tag)
			setLocation(data.getActivity.location)
			setDate(data.getActivity.activityDate)
			setGroupSize(data.getActivity.groupSize)
			setPrice(data.getActivity.price)
			setImageLocation(data.getActivity.imageLocation)
			setHostTitle(data.getActivity.hostTitle)
			setHostDescription(data.getActivity.hostDescription)
			setHostName(data.getActivity.hostName)
			setDuration(data.getActivity.duration)
		})
	}, [])

    if (isRedirected) {
        return <Redirect to="/user-transactions"/>
	}

	const bookActivity = (paymentMode) => {
		let variables = {
			userId: user.id,
			userName: user.name,
			activityId: props.match.params.id,
			activityName: aName,
			activityTag: aTag,
			activityDate: aDate,
			activityPrice: parseFloat(aPrice),
			activityLocation: aLocation, 
			paymentMode: paymentMode
		}

		GQLClient(options).request(Mutation.bookActivity, variables).then( (data) => {
			if (data.bookActivity) {
				Swal.fire({
					title: 'Activity Booked',
					text: 'You will be redirected to the menu after closing this message',
					icon: 'success'
				}) 
			} else {
				Swal.fire({
					title: 'Item Add Failed',
					text: 'The server encountered an error.',
					icon: 'error'
				})
			}
		}).then()

		setIsRedirected(true)
	}

	return(
		<Container fluid id={style.pageContainer}>
			<Row>
				<Col sm="auto" md={{offset: 1, span: 7}}>
					<h1 id={style.header}>Review your booking</h1>
					<Card>
						<Card.Body id={style.cardBody}>
							<BookingForm 
								setIsRedirected={setIsRedirected}
								aName={aName}
								aDescription={aDescription}
								aTag={aTag}
								aLocation={aLocation}
								aDate={aDate}
								aGroupSize={aGroupSize}
								aPrice={aPrice}
								aImageLoc={aImageLoc}
								hTitle={hTitle}
								hDescription={hDescription}
								hName={hName}
								aDuration={aDuration}
							/>
						</Card.Body>
					</Card>
				</Col>
				<Col sm="auto" md={2}>
					<Card id={style.cardButtonsContainer}>
						<Card.Header id={style.cardHeader}>Confirm Payment Mode</Card.Header>
						<Card.Body>
							<ConfirmPaymentCard aPrice={aPrice} bookActivity={bookActivity} setIsRedirected={setIsRedirected} />
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

const ConfirmPaymentCard = ({bookActivity, aPrice}) => {
	return (
		<React.Fragment>
			<text id={style.cardPrice}>&#8369;{aPrice}/person</text>

			<Button block toggle id={style.cardButton} size="sm" variant="secondary" onClick={ (e) => bookActivity('Cash on Meet') }>Pay with Cash</Button>
			<Button block toggle id={style.cardButton} size="sm" variant="secondary" onClick={ (e) => bookActivity('Stripe') }>Pay with Stripe</Button>

		</React.Fragment>
	)
}

const BookingForm = ({aImageLoc, aName, aDescription, aTag, aLocation, aDate, aGroupSize, aPrice, hTitle, hDescription, hName, aDuration}) => {
	return(
		<Row>
			<Col md={5}>
				<img id={style.cardImage} alt="activity preview" src={ NodeServerURL + aImageLoc }/>
			</Col>

			<Col md={7} id={style.cardContainer} noGutters>
				<Row md={12} id={style.cardHeader} noGutters>
					<h2>{aName}</h2>
				</Row>
				<Row id={style.cardSubHeader} noGutters>
					<Col md={5}>
						<div>
							<span>{aLocation} | {aTag}</span>
						</div>
					</Col>
					<Col md={7} id={style.cardDate}>{aDate}</Col>
				</Row>
				<hr id={style.horizontalLine}></hr>
				<Row id={style.cardDescriptionContainer} noGutters>
					<Col md={3} id={style.cardWhat}>What you'll do</Col>
					<Col md={9} id={style.cardDescription}>{aDescription}</Col>
				</Row>
				<Row id={style.cardHostDetailsContainer} noGutters>
					<Col md={3} id={style.cardWho}>Your host</Col>
					<Col md={3} id={style.cardHostName}>{hName}</Col>
					<Col md={6} id={style.cardHostTitle}>{hTitle}</Col>
				</Row>
				<Row id={style.cardHostDescriptionContainer} noGutters>
					<Col md={{span: 9, offset: 3}} id={style.cardHostDescription}>{hDescription}</Col>
				</Row>
				<Row id={style.cardIconsContainer} noGutters>
					<Col md={{span: 9, offset: 3}}>
						<Row>
							<Col md={3}>
								<i id={style.cardIcons} class="fas fa-users"></i>
							</Col>
							<Col md={3}>
								<i id={style.cardIcons} class="fas fa-clock"></i>	
							</Col>
							<Col md={5}>
								<i id={style.cardIcons} class="fas fa-clipboard-list"></i>
							</Col>
						</Row>
					</Col>
				</Row>
				<Row id={style.cardMinorDetailsContainer} noGutters>
					<Col md={{span: 9, offset: 3}}>
						<Row>
							<Col md={3}>
								<span id={style.cardMinorDetailsHeader}>Group Size</span><br/>
								<span id={style.cardMinorDetailsContent}>{aGroupSize}</span>
							</Col>
							<Col md={3}>
								<span id={style.cardMinorDetailsHeader}>Duration</span><br/>
								<span id={style.cardMinorDetailsContent}>{aDuration}</span>
							</Col>
							<Col md={5}>
								<span id={style.cardMinorDetailsHeader}>Includes</span><br/>
								<span id={style.cardMinorDetailsContent}>Food and equipment</span>
							</Col>
						</Row>
					</Col>
				</Row>
			</Col>
		</Row>
	)
}

export default BookingPage