import React from 'react'
import style from '../style/BookingPage.module.css' 

import Container from 'react-bootstrap/Container'

const NotFoundPage = () => {
    return(
        <Container id={style.pageContainer}>
            <h1>Page not found</h1>
        </Container>
    )
}

export default NotFoundPage