import React, { useEffect, useContext, useState } from 'react'
import Query from '../graphql/queries'
import { UserContext } from '../contexts/UserContext'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import { GQLClient } from '../helpers'

import style from '../style/UserTransactionsPage.module.css'

const UserTransactions = () => {
    const {user} = useContext(UserContext)

    return(
        <Container id={style.pageContainer}>
			<Card>
				<Card.Header>Your transactions</Card.Header>
				<TransactionsTable user={user}/>
			</Card>
		</Container>
    )
}

const TransactionsTable = ({user}) => {
	
    const [transactions, setTransactions] = useState([])
    
    useEffect( () => {
        GQLClient({}).request(Query.getTransactions, { id: user.id }).then( ({getTransactions}) => {
            setTransactions(getTransactions.map( (transaction) => 
                <tr>
                    <td>{transaction.activityName} </td>
                    <td>{transaction.activityDate} </td>
                    <td>{transaction.activityLocation} </td>
                    <td>{transaction.paymentMode} </td>
                    <td>{transaction.activityPrice} </td>
                </tr>
            ))
        })
    }, [])
    
    return(
        <table class="table table-light table-striped">
            <thead class="thead-light">
                <tr>
                    <th id={style.columnHeader}>Activity Name</th>
                    <th id={style.columnHeader}>Date</th>
                    <th id={style.columnHeader}>Location</th>
                    <th id={style.columnHeader}>Payment Method</th>
                    <th id={style.columnHeader}>Price</th>
                </tr>
            </thead>
            <tbody id={style.tableBody}>
                {transactions}
            </tbody>
        </table>
    )
}

export default UserTransactions