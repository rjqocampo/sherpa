import React, {useState, useEffect} from 'react'
import Query from '../graphql/queries'
import {GQLClient} from '../helpers'
import Mutation from '../graphql/mutations'
import {Redirect} from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import style from '../style/EditActivityPage.module.css'

const EditActivityPage = (props) => {
    const [isRedirected, setIsRedirected] = useState('')

    if (isRedirected) {
        return <Redirect to='/' />
    }

    return(
        <Container id={style.pageContainer}>
            <h3>Edit Activity</h3>
            <Card>
                <Card.Header>Activity & Host Details</Card.Header>
                <Card.Body>
                    <EditActivityForm activityId={props.match.params.id} setIsRedirected={setIsRedirected}/>
                </Card.Body>
            </Card>
        </Container>
    )
}

const EditActivityForm = ({activityId, setIsRedirected}) => {
    const [name, setName] = useState('')
	const [location, setLocation] = useState('')
	const [description, setDescription] = useState('')
	const [groupSize, setGroupSize] = useState(0)
	const [duration, setDuration] = useState('')
	const [price, setPrice] = useState(0)
	const [activityDate, setActivityDate] = useState('')
	const [tag, setTag] = useState(undefined)
	const [tags, setTags] = useState([])
	const [hostName, setHostName] = useState('')
	const [hostTitle, setHostTitle] = useState('')
    const [hostDescription, setHostDescription] = useState('')

    useEffect( () => {
        GQLClient({}).request(Query.getTags, null).then( (data) => {
            let options = data.getTags.map( (tag) => {
                return <option key={tag.id} value={tag.name}>{tag.name}</option>
            })
        
            setTags(options)
        })

        GQLClient({}).request(Query.getActivity, {id: activityId}).then( (data) => {
            setName(data.getActivity.name)
            setLocation(data.getActivity.location)
            setDescription(data.getActivity.description)
            setDuration(data.getActivity.duration)
            setPrice(data.getActivity.price)
            setActivityDate(data.getActivity.activityDate)
            setGroupSize(data.getActivity.groupSize)
            setTag(data.getActivity.tag)
            setHostName(data.getActivity.hostName)
            setHostTitle(data.getActivity.hostTitle)
            setHostDescription(data.getActivity.hostDescription)
        })
    }, [])
    
    const EditActivity = (e) => {
        e.preventDefault()

        let variables = {
            id: activityId,
            name,
            location,
            description,
            groupSize: parseFloat(groupSize),
            duration,
            price: parseFloat(price),
            activityDate, 
            tag,
            hostName,
            hostTitle,
            hostDescription
        }

        GQLClient({}).request(Mutation.editActivity, variables)

        setIsRedirected(true)
    }
    
    return(
        <Form onSubmit={ (e) => EditActivity(e) } className="row">
			<div className="col-6">
				<Form.Group>
					<Form.Label>Name</Form.Label>
					<Form.Control type="text" value={name} onChange={ (e) => setName(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Location</Form.Label>
					<Form.Control type="text" value={location} onChange={ (e) => setLocation(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control type="text" value={description} onChange={ (e) => setDescription(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Duration</Form.Label>
					<Form.Control type="text" value={duration} onChange={ (e) => setDuration(e.target.value) }/>
				</Form.Group>
				<div className="row">
					<Form.Group className="col-6">
						<Form.Label>Group Size</Form.Label>
						<Form.Control type="number" value={groupSize} onChange={ (e) => setGroupSize(e.target.value) }/>
					</Form.Group>
					<Form.Group className="col-6">
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" value={price} onChange={ (e) => setPrice(e.target.value) }/>
					</Form.Group>
				</div>
				<Form.Group>
					<Form.Label>Date of Activity</Form.Label>
					<Form.Control type="text" value={activityDate} onChange={ (e) => setActivityDate(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Type of Activity</Form.Label>
					<Form.Control value={tag} onChange={ (e) => setTag(e.target.value) } as="select">
						<option value selected disabled>Select a category</option>
						{tags}
					</Form.Control>
				</Form.Group>
			</div>

			<div className="col-6">
				<Form.Group>
					<Form.Label>Host Name</Form.Label>
					<Form.Control type="text" value={hostName} onChange={ (e) => setHostName(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Host Title</Form.Label>
					<Form.Control type="text" value={hostTitle} onChange={ (e) => setHostTitle(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Host Description</Form.Label>
					<Form.Control type="text" value={hostDescription} onChange={ (e) => setHostDescription(e.target.value) }/>
				</Form.Group>
			</div>
			

			<Button type="submit" variant="success">Edit</Button>&nbsp;
			<Button type="button" variant="warning" href="/">Cancel</Button>
		</Form>
    )
}

export default EditActivityPage