import React, {useState, useEffect} from 'react'
import {Redirect} from 'react-router-dom'
import {GQLClient} from '../helpers'
import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'

import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import style from '../style/DeleteActivityPage.module.css'

const DeleteActivityPage = (props) => {
    const [isRedirected, setIsRedirected] = useState(false)

    if (isRedirected) {
        return <Redirect to='/'/>
    }


    return(
        <Container id={style.pageContainer}>
            <h3>Delete Item</h3>
            <Card>
                <Card.Header>Item Information</Card.Header>
                <Card.Body>
                    <ActivityDeleteForm activityId={props.match.params.id} setIsRedirected={setIsRedirected} />
                </Card.Body>
            </Card>
        </Container>
    )
}

const ActivityDeleteForm = ({activityId, setIsRedirected}) => {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [tag, setTag] = useState('')

    useEffect( () => {
        GQLClient({}).request(Query.getActivity, { id: activityId }).then( (data) => {
            setName(data.getActivity.name)
            setDescription(data.getActivity.description)
            setTag(data.getActivity.tag)
        })
    }, [])

    const deleteItem = (e) => {
        e.preventDefault()

        GQLClient().request(Mutation.deleteActivity, { id: activityId })

        setIsRedirected(true)
    }

    return(
        <Form onSubmit={ (e) => deleteItem(e)}>
            <Form.Group>
                <Form.Label>Activity Name</Form.Label>
                <Form.Control type="text" value={name} disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control type="text" value={description} disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Type of Activity</Form.Label>
                <Form.Control type="text" value={tag} disabled/>
            </Form.Group>

            <Button type="submit" variant="danger">Delete</Button>&nbsp;
            <Button type="button" variant="warning" href="/">Cancel</Button>
        </Form>
    )
}

export default DeleteActivityPage