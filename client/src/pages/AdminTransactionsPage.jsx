import React, { useEffect, useContext, useState } from 'react'
import Query from '../graphql/queries'
import { UserContext } from '../contexts/UserContext'
import { GQLClient } from '../helpers'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

import style from '../style/AdminTransactionsPage.module.css'

const AdminTransactions = () => {
    return(
        <Container id={style.pageContainer}>
			<Card>
				<Card.Header>User transactions</Card.Header>
				<Card.Body>
					<TransactionsTable />
				</Card.Body>
			</Card>
		</Container>
    )
}

const TransactionsTable = () => {	
    const [users, setUsers] = useState([])

    useEffect(() => {
        GQLClient({}).request(Query.getUsers, null).then(({ getUsers }) => {
            setUsers(getUsers.map((user) => <AccordionEntry user={ user }/>))
        })
    }, [])
    
    return (
        <Accordion defaultActiveKey="0">
            {users}
        </Accordion>
    )
}

const AccordionEntry = ({ user }) => {
    const [accordion, setAccordion] = useState([])

    const transactionDetails = (e, name) => {
        GQLClient({}).request(Query.getTransactionsByName, { name: name }).then( ({getTransactionsByName}) => {
            setAccordion(getTransactionsByName.map( (transaction) => 
                <Accordion.Collapse eventKey={transaction.userName}>
                    <Card.Body>
                        <Row>
                        <span className="col-3">#{transaction.id}</span>
                        <span className="col-2">{transaction.activityName}</span>
                        <span className="col-2">Location: {transaction.activityLocation}</span>
                        <span className="col-2">Date: {transaction.activityDate}</span>
                        <span className="col-2">Payment Mode: {transaction.paymentMode}</span>
                        <span className="col-1">Price: {transaction.activityPrice}</span>
                        </Row>
                    </Card.Body>
                </Accordion.Collapse>
            ))
        })
    }

    return (
        <Card>
            <Accordion.Toggle as={Card.Header} variant="link" eventKey={user.userName} onClick={ (e) => transactionDetails(e, user.userName) }>
                {user.userName}
            </Accordion.Toggle>

            {accordion}
        </Card>
    )
}

export default AdminTransactions