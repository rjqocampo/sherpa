import React, {useState, useEffect} from 'react'
import {Redirect} from 'react-router-dom'
import {GQLClient, toBase64} from '../helpers'
import Mutation from '../graphql/mutations'
import Query from '../graphql/queries'

import Swal from 'sweetalert2'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import style from '../style/CreateActivityPage.module.css'

const ActivityCreatePage = () => {
	const [isRedirected, setIsRedirected] = useState(false)

	if (isRedirected) {
		return <Redirect to="/"/>
	}
	
	return( 
		<Container id={style.pageContainer}>
			<Card>
				<Card.Header>Create Activity</Card.Header>
				<Card.Body>
					<ActivityCreateForm setIsRedirected={setIsRedirected} />
				</Card.Body>
			</Card>
		</Container>
	)
}

const ActivityCreateForm = ({setIsRedirected}) => {
	const [name, setName] = useState('')
	const [location, setLocation] = useState('')
	const [description, setDescription] = useState('')
	const [groupSize, setGroupSize] = useState(0)
	const [duration, setDuration] = useState('')
	const [price, setPrice] = useState(0)
	const [activityDate, setActivityDate] = useState('')
	const [tag, setTag] = useState(undefined)
	const [tags, setTags] = useState([])
	const [hostName, setHostName] = useState('')
	const [hostTitle, setHostTitle] = useState('')
	const [hostDescription, setHostDescription] = useState('')
	const fileRef = React.createRef()

	useEffect( () => {
		GQLClient({}).request(Query.getTags, null).then( (data) => {
			let options = data.getTags.map( (data) => {
				return <option key={data.id} value={data.name}>{data.name}</option>
			})

			setTags(options)
		})
	}, [])

	const createActivity = (e) => {
		e.preventDefault()

		toBase64(fileRef.current.files[0]).then((encodedFile) => {
			let variables = {
				name,
				location,
				description,
				groupSize: parseFloat(groupSize),
				duration,
				price: parseFloat(price),
				activityDate, 
				tag,
				base64EncodedImage: encodedFile,
				hostName,
				hostTitle,
				hostDescription
			}

			GQLClient({}).request(Mutation.addActivity, variables).then( (data) => {
				if (data.addActivity) {
                    Swal.fire({
                        title: 'Item Added',
                        text: 'You will be redirected to the menu after closing this message',
                        icon: 'success'
                    }).then(() => {
                        setIsRedirected(true)
                    })    
                } else {
                    Swal.fire({
                        title: 'Item Add Failed',
                        text: 'The server encountered an error.',
                        icon: 'error'
                    })
                }
			})
		})
	}


	return(
		<Form onSubmit={ (e) => createActivity(e) } className="row">
			<div className="col-6">
				<Form.Group>
					<Form.Label>Name</Form.Label>
					<Form.Control type="text" value={name} onChange={ (e) => setName(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Location</Form.Label>
					<Form.Control type="text" value={location} onChange={ (e) => setLocation(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control type="text" value={description} onChange={ (e) => setDescription(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Duration</Form.Label>
					<Form.Control type="text" value={duration} onChange={ (e) => setDuration(e.target.value) }/>
				</Form.Group>
				<div className="row">
					<Form.Group className="col-6">
						<Form.Label>Group Size</Form.Label>
						<Form.Control type="number" value={groupSize} onChange={ (e) => setGroupSize(e.target.value) }/>
					</Form.Group>
					<Form.Group className="col-6">
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" value={price} onChange={ (e) => setPrice(e.target.value) }/>
					</Form.Group>
				</div>
				<Form.Group>
					<Form.Label>Date of Activity</Form.Label>
					<Form.Control type="text" value={activityDate} onChange={ (e) => setActivityDate(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Type of Activity</Form.Label>
					<Form.Control value={tag} onChange={ (e) => setTag(e.target.value) } as="select">
						<option value selected disabled>Select a category</option>
						{tags}
					</Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Image</Form.Label>
					<Form.Control type="file" className="form-control" ref={ fileRef } accept="images/png" />
				</Form.Group>
			</div>

			<div className="col-6">
				<Form.Group>
					<Form.Label>Host Name</Form.Label>
					<Form.Control type="text" value={hostName} onChange={ (e) => setHostName(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Host Title</Form.Label>
					<Form.Control type="text" value={hostTitle} onChange={ (e) => setHostTitle(e.target.value) }/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Host Description</Form.Label>
					<Form.Control type="text" value={hostDescription} onChange={ (e) => setHostDescription(e.target.value) }/>
				</Form.Group>
			</div>
			

			<Button type="submit" variant="success">Add</Button>&nbsp;
			<Button type="button" variant="warning" href="/">Cancel</Button>
		</Form>
	)
}

export default ActivityCreatePage