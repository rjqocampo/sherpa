import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import Mutation from '../graphql/mutations'
import { GQLClient } from '../helpers'

import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Swal from 'sweetalert2'

import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';

import style from '../style/RegisterPage.module.css'

const RegisterPage = (props) => {
    const [isRedirected, setIsRedirected] = useState(false)

    if (isRedirected) {
        return <Redirect to="/"/>
    }
    
    return(
        <Container fluid>
            <RegisterForm setIsRedirected={setIsRedirected}/>
        </Container>
    )
}

const RegisterForm = ({setIsRedirected}) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState(true)

    const register = (e) => {
        e.preventDefault()

        let variables = { name: name, email: email, password: password }

        GQLClient({}).request(Mutation.addUser, variables)
        .then(data => {
            const userAdded = data.addUser

            if (userAdded) {
                Swal.fire('Registration Successful', 'You will now be redirected to the login.', 'success').then( () => {
                    setIsRedirected(true)
                })
            } else {

            }
        })
    }

    const checkPassword = (password) => {
        setPassword(password)

        if (password.length >= 8) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }
    }

    return(
        <Row id={style.pageContainer}>
            <Col id={style.column}>
            <Card id={style.cardContainer}>
                <h1 id={style.header}>Create Your Account</h1>
                <Card.Body id={style.cardBody}>
                    <Form onSubmit={ (e) => register(e) }>
                        <Form.Group>
                            <Form.Label id={style.inputLabel}>Full Name</Form.Label>
                            <Form.Control type="text" value={ name } onChange={ (e) => setName(e.target.value) }/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label id={style.inputLabel}>Email</Form.Label>
                            <Form.Control type="email" value={ email } onChange={ (e) => setEmail(e.target.value) }/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label id={style.inputLabel}>Password</Form.Label>
                            <Form.Control type="password" value={ password } onChange={ (e) => checkPassword(e.target.value) }/>
                        </Form.Group>
                        <h6 id={style.withAccount}>Already have an account? <a id={style.clickHere} href="/">Click here.</a></h6>
                        <div id={style.buttonContainer}>
                            <Button  id={style.button}type="submit" variant="success" disabled={isDisabled}>Sign up</Button>&nbsp;
                        </div>
                    </Form>
                </Card.Body>
            </Card>
            </Col>
        </Row>
    )
}

export default RegisterPage