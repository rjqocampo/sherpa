import React, {useState, useContext} from 'react'
import {UserContext} from '../contexts/UserContext'
import Mutation from '../graphql/mutations'
import {Redirect} from 'react-router-dom'

import Card from 'react-bootstrap/Card'
import Container from 'react-bootstrap/Container'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { GQLClient } from '../helpers'
import Swal from 'sweetalert2'
import style from '../style/LoginPage.module.css'

const LoginPage = () => {
    const [isRedirected, setIsRedirected] = useState(false)

    if(isRedirected) {
        return <Redirect to="/"/>
    }

    return(
        <Container>
            <Card id={style.loginCard}>
                <h3 className="p-4" id={style.loginHeader}>Meet hosts around the world and experience one-of-a-kind activities and adventures</h3>
                <Card.Body>
                    <LoginForm setIsRedirected={setIsRedirected}/>
                </Card.Body>
            </Card>
        </Container>
    )
}

const LoginForm = ({setIsRedirected}) => {
    const { setUser } = useContext(UserContext)
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const login = (e) => {
        e.preventDefault()

        let variables = { email: username, password}

        GQLClient({}).request(Mutation.login, variables).then( (data) => {
            const result = data.login

            if (result !== null) {
                localStorage.setItem('id', result.id)
                localStorage.setItem('name', result.name)
                localStorage.setItem('role', result.role)
                localStorage.setItem('token', result.token)

                setUser({
                    id: localStorage.getItem('id'),
                    name: localStorage.getItem('name'),
                    role: localStorage.getItem('role'),
                    token: localStorage.getItem('token')
                })

                setIsRedirected(true)
            } else {
                Swal.fire({
                    title: 'Login Failed',
                    text: 'Either your email or password is incorrect, please try again.',
                    type: 'error'
                })
            }
        })
    }

    return (
        <Form onSubmit={ (e) => login(e) }>
            <Form.Group>
                <Form.Label id={style.inputLabel}>USERNAME</Form.Label>
                <Form.Control type="text" value={username} onChange={ (e) => setUsername(e.target.value) } />
            </Form.Group>
            <Form.Group>
                <Form.Label id={style.inputLabel}>PASSWORD</Form.Label>
                <Form.Control type="password" value={password} onChange={ (e) => setPassword(e.target.value) } />
            </Form.Group>
            <h6 id={style.noAccount}>Don't have an account yet? <a id={style.clickHere} href="/register">Click here.</a></h6>
            <Button id={style.button} size="sm" type="submit" variant="success">Login</Button>
        </Form>
    )
}



export default LoginPage