const Mutation = {
    addUser:`
        mutation(
            $name: String!,
            $email: String!,
            $password: String!
        ) {
            addUser (
                name: $name,
                email: $email,
                password: $password
            )
        }
    `,
    login:`
        mutation(
            $email: String!,
            $password: String!
        ) {
            login (
                email: $email,
                password: $password
            ) {
                id
                name
                role
                token
            }
        }
    `,
    addActivity:`
        mutation(
            $name: String!,
            $location: String!,
            $description: String!,
            $groupSize: Float!,
            $duration: String!,
            $price: Float!,
            $activityDate: String!,
            $tag: String!,
            $base64EncodedImage: String!,
            $hostName: String!,
            $hostTitle: String!,
            $hostDescription: String
        ) {
            addActivity (
                name: $name,
                location: $location,
                description: $description,
                groupSize: $groupSize,
                duration: $duration,
                price: $price,
                activityDate: $activityDate,
                tag: $tag,
                base64EncodedImage: $base64EncodedImage,
                hostName: $hostName,
                hostTitle: $hostTitle,
                hostDescription: $hostDescription
            )
        }
    `,
    deleteActivity:`
        mutation (
            $id: String! 
        ) {
            deleteActivity (
                id: $id
            )
        } 
    `,
    editActivity:`
        mutation(
            $id: String!,
            $name: String!,
            $location: String!,
            $description: String!,
            $groupSize: Float!,
            $duration: String!,
            $price: Float!,
            $activityDate: String!,
            $tag: String!,
            $hostName: String!,
            $hostTitle: String!,
            $hostDescription: String
        ) {
            editActivity (
                id: $id,
                name: $name,
                location: $location,
                description: $description,
                groupSize: $groupSize,
                duration: $duration,
                price: $price,
                activityDate: $activityDate,
                tag: $tag,
                hostName: $hostName,
                hostTitle: $hostTitle,
                hostDescription: $hostDescription
            ) {
                name
            }
        }
    `,
    addToCart:`
        mutation(
            $userId: String!,
            $activityId: String!,
            $activityName: String!,
            $activityPrice: Float!,
            $activityDate: String!,
            $activityLocation: String!,
            $activityImageLocation: String!
        ) {
            addToCart (
                userId: $userId,
                activityId: $activityId,
                activityName: $activityName,
                activityPrice: $activityPrice,
                activityDate: $activityDate,
                activityLocation: $activityLocation,
                activityImageLocation: $activityImageLocation
            )
        }
    `,
    deleteCart:`
        mutation(
            $cartId: String!
        ) {
            deleteCart (
                cartId: $cartId
            ) {
                activityName
            }
        }
    `,
    bookActivity:`
        mutation(
            $userId: String!,
            $userName: String!,
			$activityId: String!,
			$activityName: String!,
			$activityTag: String!,
			$activityDate: String!,
			$activityPrice: Float!,
            $activityLocation: String!,
            $paymentMode: String!
        ) {
            bookActivity (
                userId: $userId,
                userName: $userName,
                activityId: $activityId,
                activityName: $activityName,
                activityTag: $activityTag,
                activityDate: $activityDate,
                activityPrice: $activityPrice,
                activityLocation: $activityLocation,
                paymentMode: $paymentMode
            )
        }
    `

}

export default Mutation