const Query = {
	getTags:`
		{
			getTags{
				id
				name
			}
		}
	`,
	getActivities:`
		{
			getActivities{
				id
				name
				location
				activityDate
				description
				price
				groupSize
				duration
				tag
				imageLocation
				hostName
				hostTitle
				hostDescription
			}
		}
	`,
	getActivity:`
		query (
			$id: String!
		) {
			getActivity (
				id: $id
			) {
				name
				description
				tag
				location
				duration
				groupSize
				price
				activityDate
				hostName
				hostTitle
				hostDescription
				imageLocation
			}
		}
	`,
	getUserCart:`
		query (
			$id: String!
		) {
			getUserCart (
				id: $id
			) {
				id
				userId
				activityId
				activityName
				activityDate
				activityPrice
				activityLocation
			}
		}
	`,
	getTransactions:`
		query (
			$id: String!
		) {
			getTransactions (
				id: $id
			) {
				activityName
				activityTag
				activityDate
				activityPrice
				activityLocation
				paymentMode
			}
		}
	`,
	getAllTransactions:`
		{
			getAllTransactions{
				id
				userName
				activityName
				activityTag
				activityDate
				activityPrice
				activityLocation
				paymentMode
			}
		}
	`,
	getUsers:`
		{
			getUsers {
				userName
			}
		}
	`,
	getTransactionsByName:`
	query (
		$name: String!
	) {
		getTransactionsByName (
			name: $name
		) {
			id
			userName
			activityName
			activityDate
			activityTag
			activityPrice
			activityLocation
			paymentMode
		}
	}
	`
}

export default Query