import React, {useContext, useState, useEffect} from 'react'
import {Redirect} from 'react-router-dom'
import {UserContext} from '../contexts/UserContext'
import {GQLClient} from '../helpers'
import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Button from 'react-bootstrap/Button'
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
	MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon } from "mdbreact";


import style from '../style/AppNavbar.module.css'

const AppNavbar = (props) => {
	const { user } = useContext(UserContext)
	const [cartItems, setCartItems] = useState([])
	let navigation 

	const deleteCart = (e, id) => {
		e.preventDefault()
		e.stopPropagation()

	    GQLClient({}).request(Mutation.deleteCart, { cartId: id }).then(
			getCartItems()
		)
	}

	const getCartItems = () => {
		GQLClient({}).request(Query.getUserCart, { id: user.id }).then( ({getUserCart}) => {
			const cartRows = getUserCart.map( (cart) => 
				<MDBDropdownItem key={cart._id}>
					<span className={style.cartRow}>
						{cart.activityName}
					</span>
					<Button type="button" onClick={ (e) => deleteCart(e, cart.id) } className="p-1" id={style.unfollowButtonContainer}>
						<i class="fas fa-times" id={style.unfollowButton}></i>
					</Button>
				</MDBDropdownItem>
			)

			setCartItems(cartRows)
		})
	}

	if (user.token === null) {
		navigation = (
			<Nav className={style.navLinksGroup}>
				<Nav.Link className={style.navLink} href="/register">register</Nav.Link>
				<Nav.Link className={style.navLink} href="/">login</Nav.Link>
			</Nav>
		)
	} else {
		if (user.role === 'admin') {
			navigation = (
				<React.Fragment>
					<Nav className={style.navLinksGroup}>
						<Nav.Link className={style.navLink} href="/">menu</Nav.Link>
						<Nav.Link className={style.navLink} href="/admin-transactions">transactions</Nav.Link>
						<Nav.Link className={style.navLink} href="/logout">logout</Nav.Link>
					</Nav>
				</React.Fragment>
			)
		} else {
			navigation = (
				<React.Fragment>
					<Nav className={style.navLinksGroup}>
						<Nav.Link className={style.navLink} href="/">menu</Nav.Link>
						
						<MDBNavItem>
							<MDBDropdown>
								<MDBDropdownToggle nav caret className={style.navLink} onClick={ () => getCartItems() }>
									<div className="d-none d-md-inline">following</div>
								</MDBDropdownToggle>

								<MDBDropdownMenu right basic>
									{cartItems}
									<MDBDropdownItem divider />
									<MDBDropdownItem>
										<a href="/follow-list" className={style.cartRow}>View Detailed List</a>
									</MDBDropdownItem>
								</MDBDropdownMenu>

							</MDBDropdown>
						</MDBNavItem>

						<Nav.Link className={style.navLink} href="/user-transactions">transactions</Nav.Link>
						<Nav.Link className={style.navLink} href="/logout">logout</Nav.Link>
					</Nav>
				</React.Fragment>
			)
		}
	}
	

	return (
		<MDBNavbar id={style.navbar} color="#ffffff white" fixed="top" dark expand="md" scrolling transparent>
			<Navbar.Brand id={style.brandName}>sherpa</Navbar.Brand>
			<Navbar.Collapse>
				{ navigation }
			</Navbar.Collapse>
		</MDBNavbar>
	)
}

export default AppNavbar