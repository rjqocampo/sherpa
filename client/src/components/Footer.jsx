import React from 'react'

import style from '../style/Footer.module.css'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'

const Footer = () => {
    return (
        <React.Fragment>
            <Container fluid>
                <Row id={style.row1}>
                    <Col md={{offset: 3, span: 2}}>
                        <h6 id={style.footerSignUp}>SIGN UP</h6>
                        <h6 id={style.footerSubscribe}>Subscribe to our newsletter to receive the latest news and promos every week</h6>
                    </Col>
                    <Col md={{offset: 1, span: 3}}>
                        <Form>
                            <InputGroup id={style.formContainer} className="mb-3">
                                <FormControl
                                    placeholder="Your email address"
                                    aria-label="Your email"
                                    aria-describedby="basic-addon2"
                                />
                                <InputGroup.Append>
                                    <Button id={style.footerButton} type="submit" size="sm">Submit</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
            <Container fluid id={style.container2}>
                <Row id={style.row2}>
                    <Col md={{offset: 2, span: 3}}>
                        <h6 id={style.footerBrand}>sherpa</h6>

                        <div id={style.footerBrandContainer1}>
                            <h6 id={style.footerBrandAbout}>About us</h6>
                            <h6 id={style.footerBrandAboutContent}>We want to bridge people and the things they want to do seamlessly</h6>
                        </div>

                        <div id={style.footerBrandContainer2}>
                            <h6 id={style.footerBrandContact}>Contact us</h6>
                            <ul>
                                <li id={style.footerBrandContactContent}>+987 654 3210</li>
                                <li id={style.footerBrandContactContent}>sherpa@example.com</li>
                            </ul>
                        </div>
                    </Col>
                    <Col md={{offset: 1, span: 2}}>
                        <h6 id={style.footerLinksHeader}>NAVIGATE</h6>
                        <ul id={style.bulletContainer1}>
                            <li id={style.footerBullets}><a id={style.footerLinks}href="#">Home</a></li>
                            <li id={style.footerBullets}><a id={style.footerLinks}href="#">About</a></li>
                            <li id={style.footerBullets}><a id={style.footerLinks}href="#">Careers</a></li>
                            <li id={style.footerBullets}><a id={style.footerLinks}href="#">Blog</a></li>
                        </ul>
                        <h6 id={style.footerLinksHeader}>LEGAL</h6>
                        <ul id={style.footerBullets}>
                            <li id={style.footerBullets}><a id={style.footerLinks} href="#">Terms and Conditions</a></li>
                            <li id={style.footerBullets}><a id={style.footerLinks} href="#">Privacy and Policy</a></li>
                        </ul>
                    </Col>
                    <Col md={{span: 2}}>
                        <div id={style.footerContainer3}>
                            <h6 id={style.footerLinksHeader}>CONNECT WITH US</h6>
                            <div id={style.iconContainer}>
                                <a id={style.footerIconsHover} href="#"><i id={style.footerIcons} class="fab fa-facebook"></i></a>
                                <a id={style.footerIconsHover} href="#"><i id={style.footerIcons} class="fab fa-twitter"></i></a>
                                <a id={style.footerIconsHover} href="#"><i id={style.footerIcons} class="fab fa-instagram"></i></a>
                                <a id={style.footerIconsHover} href="#"><i id={style.footerIcons} class="fab fa-youtube"></i></a>
                            </div>
                        </div>
                    </Col>
                    <Col md={{offset: 1, span: 10}}>
                    <hr id={style.horizontalLine}></hr>
                    <h1 id={style.copyright}>&copy; 2020 Sherpa All Rights Reserved</h1>
                    </Col>
                </Row>

            </Container>
        </React.Fragment>
    )
}

export default Footer