import React, { useState } from 'react';
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import { UserProvider } from './contexts/UserContext'

import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer'
import MenuPage from './pages/MenuPage'
import RegisterPage from './pages/RegisterPage'
import LoginPage from './pages/LoginPage'
import NotFoundPage from './pages/NotFoundPage'
import CreateActivityPage from './pages/CreateActivityPage'
import DeleteActivityPage from './pages/DeleteActivityPage'
import EditActivityPage from './pages/EditActivityPage'
import CartPage from './pages/CartPage'
import BookingPage from './pages/BookingPage'
import UserTransactionsPage from './pages/UserTransactionsPage'
import AdminTransactionsPage from './pages/AdminTransactionsPage'

import style from "./style/App.module.css"
import Container from 'react-bootstrap/Container'

const App = () => {
	const [user, setUser] = useState({
		id: localStorage.getItem('id'),
		name: localStorage.getItem('name'),
		role: localStorage.getItem('role'),
		token: localStorage.getItem('token')
	})

	const unsetUser = () => {
		localStorage.clear()
		setUser({ id: null, name: null, role: null, token: null })
	}

	const Load = (props, page) => {
		if (user.token === null) return <Redirect to="/login" />

		if (page === 'LogoutPage') {
			unsetUser()
			return <Redirect to="/" />
		}

		switch (page) {
			case 'MenuPage': return <MenuPage {...props}/>
			case 'LoginPage': return <MenuPage />
			case 'CreateActivityPage': return <CreateActivityPage />
			case 'DeleteActivityPage': return <DeleteActivityPage {...props}/>
			case 'EditActivityPage': return <EditActivityPage {...props}/>
			case 'CartPage': return <CartPage />
			case 'BookingPage': return <BookingPage {...props}/>

			default:
		}
  	}

  	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<BrowserRouter>
				<AppNavbar />
				<Switch>
					<Route exact path='/register' component={ RegisterPage } />
					<Route exact path='/' component={ MenuPage } />
					<Route exact path='/logout' render={ (props) => Load(props, 'LogoutPage') } />
					<Route exact path='/create' render={ (props) => Load(props, 'CreateActivityPage') } />
					<Route exact path='/activity-delete/:id' render={ (props) => Load(props, 'DeleteActivityPage') }/>
					<Route exact path='/activity-edit/:id' render={ (props) => Load(props, 'EditActivityPage') }/>
					<Route exact path='/confirm-book/:id' render={ (props) => Load(props, 'BookingPage') }/>
					<Route exact path='/follow-list' component={ CartPage }/>
					<Route exact path='/user-transactions' component={ UserTransactionsPage }/>
					<Route exact path='/admin-transactions' component={ AdminTransactionsPage }/>
					<Route component={ NotFoundPage } />
				</Switch>
				<Footer />
			</BrowserRouter>
		</UserProvider>
  )
}

export default App;
