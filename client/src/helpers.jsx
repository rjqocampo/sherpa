import { GraphQLClient } from 'graphql-request'

const GQLServerURL = 'https://ocampo-csp3-backend.herokuapp.com/graphql'
const NodeServerURL = 'https://ocampo-csp3-backend.herokuapp.com/'

const GQLClient = (options) => {
    return new GraphQLClient(GQLServerURL, options)
}

const toBase64 = (file) => new Promise((resolve, reject) => {
	const reader = new FileReader()
	reader.readAsDataURL(file)
	reader.onload = () => resolve(reader.result)
	reader.onerror = (error) => reject(error)
})

export { GQLClient, toBase64, NodeServerURL }