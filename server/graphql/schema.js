const { gql } = require ('apollo-server-express')

module.exports = gql`
	type Query {
		getActivity(id: String!): Activity
		getTags: [Tag]
		getActivities: [Activity]
		getUserCart (id: String!): [Cart]
		getTransactions (id: String!) : [Transaction]
		getAllTransactions : [Transaction]
		getUsers: [Transaction]
		getTransactionsByName (name: String!) : [Transaction]
	}

	type Mutation {
		addUser (name: String!, email: String!, password: String!): Boolean
		login (email: String!, password: String!): User
		addActivity (
			name: String!,
			location: String!,
			description: String!,
			groupSize: Float!,
			duration: String!,
			price: Float!,
			activityDate: String!,
			isArchived: Boolean,
			tag: String!,
			base64EncodedImage: String!,
			hostName: String!,
			hostTitle: String!,
			hostDescription: String
		) : Boolean
		editActivity (
			id: String!,
			name: String!,
			location: String!,
			description: String!,
			groupSize: Float!,
			duration: String!,
			price: Float!,
			activityDate: String!,
			isArchived: Boolean,
			tag: String!,
			hostName: String!,
			hostTitle: String!,
			hostDescription: String
		) : Activity 
		deleteActivity (id: String!) : Boolean
		addToCart (
			userId: String!,
			activityId: String!,
			activityName: String!,
			activityPrice: Float!,
			activityDate: String!,
			activityLocation: String!,
			activityImageLocation: String!
		) : Boolean
		deleteCart (cartId: String!) : Cart
		bookActivity (
			userId: String!,
			userName: String!,
			activityId: String!,
			activityName: String!,
			activityTag: String!,
			activityDate: String!,
			activityPrice: Float!,
			activityLocation: String!,
			paymentMode: String!
		) : Boolean
	}

	type User {
		id: ID!
		name: String!
		email: String!
		role: String!
		token: String
	}

	type Tag {
		id: ID!
		name: String!
	}

	type Activity {
		id: ID!
		name: String!
		location: String!
		description: String!
		groupSize: Float!
		duration: String!
		price: Float!
		activityDate: String!
		isArchived: Boolean
		imageLocation: String!
		tag: String!
		hostName: String!
		hostTitle: String!
		hostDescription: String!
	}

	type Cart {
		id: ID!
		userId: String!
		activityId: String!
		activityName: String!
		activityPrice: Float!
		activityDate: String!
		activityLocation: String!
	}

	type Transaction {
		id: ID!
		userId: String!
		userName: String!
		activityId: String!
		activityName: String!
		activityTag: String!
		activityDate: String!
		activityPrice: Float!
		activityLocation: String!
		paymentMode: String!
	}
`