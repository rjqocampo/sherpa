const User = require('../models/user')
const Activity = require('../models/activity')
const Tag = require('../models/tag')
const Cart = require('../models/cart')
const Transaction = require('../models/transaction')
const bcrypt = require('bcrypt')
const auth = require('../jwt-auth')
const uuid = require('uuid/v1')
const fs = require('fs')
const stripe = require('stripe')('sk_test_AAyBgPqOpzAcchnJ3ozKdwVR00Kxe2AQPZ')


module.exports = {
	Query: {
		getTags: (parent, args) => {
			return Tag.find({})
		},
		getActivities: (parent, args) => {
			return Activity.find({ isArchived: false })
		},
		getActivity: (parent, args) => {
			return Activity.findById(args.id)
		},
		getUserCart: (parent, args) => {
			return Cart.find({ userId: args.id })
		},
		getTransactions: (parent, args) => {
			return Transaction.find({ userId: args.id })
		},
		getAllTransactions: (parent, args) => {
			return Transaction.find({})
		},
		getUsers: (parent, args) => {				
			return Transaction.distinct('userName').then((usernames) => usernames).then( (usernames) => {
				let usernamesObj = usernames.map((username) => {
					return { userName: username }
				})

				return (usernamesObj)
			})
		},
		getTransactionsByName: (parent, args) => {
			return Transaction.find({ userName: args.name })
		}
	},
	Mutation: {
		addUser: (parent, args) => {
			return stripe.customers.create({
				email: args.email,
				source: 'tok_mastercard',
			}).then( (customer, err) => customer ).then((customer) => {
				let user = new User({
					name: args.name,
					email: args.email,
					password: bcrypt.hashSync(args.password, 10),
					role: 'customer',
					stripeCustomerId: customer.id
				})

				return user.save().then( (user) => {
					return (user) ? true : false
				})
			}).catch(() => {
				return false
			})
		},
		login: (parent, args) => {
			let query = User.findOne({ email: args.email })

			return query.then( (user) => user ).then( (user) => {
				if (user === null) { return null }

				let isPasswordMatched = bcrypt.compareSync(args.password, user.password)

				if (isPasswordMatched) {
					user.token = auth.createToken(user.toObject())
					return user
				} else {
					return null
				}
			})
		},
		addActivity: (parent, args) => {
			let base64Image = args.base64EncodedImage.split(';base64,').pop()
			let imageLocation = 'images/' + uuid() + '.png'

			fs.writeFile(imageLocation, base64Image, {encoding: 'base64'}, (err) => {

			})

			let activity = new Activity({
				name: args.name,
				location: args.location,
				description: args.description,
				groupSize: args.groupSize,
				duration: args.duration,
				price: args.price,
				activityDate: args.activityDate,
				tag: args.tag,
				isArchived: false,
				imageLocation,
				hostName: args.hostName,
				hostTitle: args.hostTitle,
				hostDescription: args.hostDescription
			})

			activity.save()

			return true
		},
		editActivity: (parent, args) => {
			let condition = {_id: args.id}
			let updates = {
				name: args.name,
				location: args.location,
				description: args.description,
				groupSize: args.groupSize,
				duration: args.duration,
				price: args.price,
				activityDate: args.activityDate,
				tag: args.tag,
				hostName: args.hostName,
				hostTitle: args.hostTitle,
				hostDescription: args.hostDescription
			}

			return Activity.findOneAndUpdate(condition, updates)
			.then( (activity, err) => {
				console.log(err)
				return (err)
			})
		},
		deleteActivity: (parent, args) => {
			let condition = {_id: args.id}
			let updates = { isArchived: true }

			return Activity.findOneAndUpdate(condition, updates)
			.then((activity, err) => {
				return(err) ? false : true
			})
		},
		addToCart: (parent, args) => {
			let cart = new Cart({
				userId: args.userId,
				activityId: args.activityId,
				activityName: args.activityName,
				activityDate: args.activityDate,
				activityPrice: args.activityPrice,
				activityLocation: args.activityLocation,
				activityImageLocation: args.activityImageLocation
			})

			cart.save()
			return true
		},
		deleteCart: (parent, args) => {
			let condition = {_id: args.cartId}

			return Cart.findOneAndDelete(condition, null)
			.then((cart, err) => {
				console.log(err)
				return(err)
			})
		},
		bookActivity: (parent, args, context) => {
			const stripeCustomerId = context.currentUser.stripeCustomerId
			
			if (args.paymentMode == 'Cash on Meet') {
				return checkoutCOD(args)
			} else if (args.paymentMode == 'Stripe') {
				return checkoutStripe(args, stripeCustomerId)
			} else {
				return false
			}
		}
	}
}

const checkoutCOD = (args) => {
	let transaction = new Transaction({
		userId: args.userId,
		userName: args.userName,
		activityId: args.activityId,
		activityName: args.activityName,
		activityTag: args.activityTag,
		activityDate: args.activityDate,
		activityPrice: args.activityPrice,
		activityLocation: args.activityLocation,
		paymentMode: args.paymentMode
	})

	transaction.save()
	
	return true
}

const checkoutStripe = (args, stripeCustomerId) => {
	let totalPrice = args.activityPrice

	return stripe.charges.create({
		amount: totalPrice * 100,
		description: null,
		currency: 'php',
		customer: stripeCustomerId
	}).then((charge, err) => charge).then((charge) => {
		let transaction = new Transaction({
			userId: args.userId,
			userName: args.userName,
			activityId: args.activityId,
			activityName: args.activityName,
			activityTag: args.activityTag,
			activityDate: args.activityDate,
			activityPrice: args.activityPrice,
			activityLocation: args.activityLocation,
			paymentMode: args.paymentMode
		})
	
		transaction.save()

		return true
	}).catch((err) => {
		return false
	})
}