const mongoose = require('mongoose')
const Schema = mongoose.Schema

const historySchema = new Schema ({
	activityId: {
		type: String,
		required: true
	},
	userId: {
		type: String,
		required: true
	},
	dateAvailed: {
		type: String,
		required: true		
	},
	amountPaid: {
		type: Number,
		required: true
	}
})

module.exports = mongoose.model('history', historySchema)