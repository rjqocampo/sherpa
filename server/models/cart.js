const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cartSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    activityId: {
        type: String,
        required: true
    },
    activityName: {
        type: String,
        required: true
    },
    activityPrice: {
        type: Number,
        required: true
    },
    activityDate: {
        type: String,
        required: true
    },
    activityLocation: {
        type: String,
        required: true
    },
    activityImageLocation: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('cart', cartSchema)