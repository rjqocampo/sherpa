const mongoose = require('mongoose')
const Schema = mongoose.Schema

const transactionSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: true
    },
    activityId: {
        type: String,
        required: true
    },
    activityName: {
        type: String,
        required: true
    },
    activityTag: {
        type: String,
        required: true
    },
    activityLocation: {
        type: String,
        required: true
    },
    activityDate: {
        type: String,
        required: true
    },
    activityPrice: {
        type: Number,
        required: true
    },
    paymentMode: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('transaction', transactionSchema)