const mongoose = require('mongoose')
const Schema = mongoose.Schema

const activitySchema = new Schema({
	name: {
		type: String,
		required: true
	},
	location: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	groupSize: {
		type: Number,
		required: true
	},
	duration: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	isArchived: {
		type: Boolean,
		default: false
	},
	imageLocation: {
		type: String,
		required: true
	},
	activityDate: {
		type: String,
		required: true
	},
	tag: {
		type: String,
		required: true
	},
	hostName: {
		type: String,
		required: true
	},
	hostTitle: {
		type: String,
		required: true
	},
	hostDescription: {
		type: String,
		required: true
	}
}, {
	timestamps: true
})

module.exports = mongoose.model('activity', activitySchema)
