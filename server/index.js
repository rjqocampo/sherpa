const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const { ApolloServer } = require('apollo-server-express')
const gqlSchema = require('./graphql/schema')
const gqlResolvers = require('./graphql/resolver')
const auth = require('./jwt-auth')


const app = express()
const PORT = process.env.PORT || 4000

app.use(bodyParser.json({ limit: '5mb'}))
app.use('/images', express.static('images'))

const apolloServer = new ApolloServer({
	typeDefs: gqlSchema,
	resolvers: gqlResolvers,
	context: ({ req }) => {
		const token = req.headers.authorization
		const currentUser = auth.verify(token)

		return { currentUser }
	}
})

apolloServer.applyMiddleware({ app, path: '/graphql' })

mongoose.connect('mongodb+srv://*******:*******@csp3-nktdb.mongodb.net/csp3', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log('Now connected to MongoDB Atlas online server.')
})

app.listen({ port: PORT }, () => {
	console.log('Express server now running at port ${PORT}')
})